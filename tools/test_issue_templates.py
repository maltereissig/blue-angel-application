from issue_templates import IssueTemplates

from pathlib import Path

def test_read():
    templates = IssueTemplates("SomeApp")
    issues = templates.read(Path("test_data/issues.md"))
    assert len(issues) == 2
    assert issues[0].title == "3.1.1.3: Hardware utilisation and energy demand when running a standard scenario"
    assert issues[0].description == """
/label ~"Annex 1" ~"Annex 2" ~"Annex 3"

**Requirement**

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Spreadsheet file (Annex 2)
* [ ] Measurement report (Annex 3)


/milestone SomeApp
"""
    assert issues[1].title == "3.1.1.4: Support for the energy management system"
    assert issues[1].description == """
/label ~"Annex 1"

**Requirement**: The software product must be capable of unrestricted functional use with activated energy management of the underlying system layers or the connected client systems.

* [ ] Compliance with the requirement is confirmed.

/milestone SomeApp
"""
