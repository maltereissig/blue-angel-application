Helper tools for handling issues on GitLab.

`get_issues.py` retrieves issues from GitLab

`create_issues.py` creates new issues for an application from the template in `templates/issues.md`.
