# Annex 6: Product information

## 3.1.3.3: Continuity of the software product

### Requirements

* Security updates must be provided free of charge
* Security updates for the labelled product for at least 5 years after the end of sale
* The user must be given the option of whether to install only security updates or also other (e.g. functional) updates.

### Product information in which fullfilement of requirements is documented


## 3.1.3.4: Uninstallability

### Requirement

* Residue-free uninstallability of the software

### Product information in which fullfilement of requirements is documented


## 3.1.3.5: Offline capability

### Requirement

* The functionality and availability of the software must not be negatively influenced by external factors, such as the availability of a licence server.

### Product information in which fullfilement of requirements is documented


## 3.1.3.6: Modularity

### Requirements

* Information on how individual modules of the software product can be deactivated during the installation process.
* Information on the extent to which individual modules of the software product (especially those that do not belong to the functions of the software product such as tracking, etc.) can be deactivated during the use of the software product.

### Product information in which fullfilement of requirements is documented


## 3.1.3.8: Documentation of the software product, licence conditions and terms of use

### Requirement

Information about the software product both publicly and also in combination with the product itself:

a) Description of the processes for installing and uninstalling the software
b) Description of the data import and export processes
c) Information on reducing the use of resources
d) Information on the licensing terms and terms of use to enable, where relevant, the legally compliant further development of the software product
e) Information on software support
f) Information on the handling of data, in the sense of existing data protection laws
g) Information on data security, data collection and data transmission

### Product information in which fullfilement of requirements is documented


## 3.2.1: Requirements for the further development and update of the product

### Requirement

If the product is changed (e.g. through updates), it must be ensure that the software product still complies with all of the criteria.

### Product information in which fullfilement of requirements is documented
