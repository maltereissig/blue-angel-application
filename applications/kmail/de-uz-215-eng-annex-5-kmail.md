# Annex 5: Open standards

## 3.1.3.2: Transparency of the software product

### Requirement

* The APIs should correspond to open standards.

## Documentation

*Interface documentation, permalink to the software source code on a source code management platform, software licences or similar*

KMail is released as open source code under the GPL v2 license. Its code always has been fully transparent, development is taking place in the open. All APIs are documented. Code can be inspected and modified by any user.

KMail is part of the KDE PIM suite and uses its standard libraries. These libraries are open source as well and all their APIs are freely accessible and documented.

* [KDE PIM API documentation](https://api.kde.org/kdepim/index.html)
* [KMail source code](https://invent.kde.org/pim/kmail)
* [KMail license](https://invent.kde.org/pim/kmail/-/blob/master/LICENSES)
