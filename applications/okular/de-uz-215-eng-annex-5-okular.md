# Annex 5: Open standards

## 3.1.3.2: Transparency of the software product

### Requirement

* The APIs should correspond to open standards.

## Documentation

The source code of Okular is released under the `GPL-2.0-or-later` license. In compliance with this license, its source code can be studied, modified and distributed by any user. 

The development of Okular is taking place in the open and - by today - 108 people have contributed to its development. Okular contributors communicate via an open mailing list and collaborate following a code of conduct. Its developers are aware of and adhere to a telemetry policy and a privacy policy. Both inform Okulars management of data.

Okular source code developers link changes to the source code to issues which highlight the rationale and goal behind the change. Linking with a source code management system simplifies studying the source code as it evolves and fosters its comprehensibility. As Okular is a standalone application for viewing documents it does not expose an API to other tools. However, Okular comes with an extension mechanism allowing developers to extend the list of document formats supported by Okular. Due to its open source, any developer motivated to extend Okulars capabilities can study and use the existing extensions for interacting with Okulars internal API. 

Users of Okular are encouraged to report errors or share their wishes for improvements for future versions of Okular. The documentation of bugfixes and improvements for Okular is organized by the community in an open bug tracker and a public source code management tool.

* [Okular API documentation](https://api.kde.org/okular/html/index.html)
* [Okular source code](https://invent.kde.org/graphics/okular)
* [Bug tracker](https://bugs.kde.org/enter_bug.cgi?product=okular)
* [Okular licenses](https://invent.kde.org/graphics/okular/-/tree/master/LICENSES)
* [Okular-devel mailing list](https://mail.kde.org/mailman/listinfo/okular-devel)
* [Code of conduct](https://kde.org/code-of-conduct/)
* [Telemetry policy](https://community.kde.org/Policies/Telemetry_Policy)
* [Privacy policy](https://kde.org/privacypolicy-apps/)
* [Okular contributors](https://invent.kde.org/graphics/okular/-/graphs/master)
* [Okular extensions](https://apps.kde.org/okular/)
